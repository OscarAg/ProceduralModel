class ProceduralTexture {

    constructor(parameters) {
        this.name = "";
        this.isProceduralTexture = true;

        // Properties that control the appearance
        this.frequency = 1.0;
        this.displacement = 0.0;
        this.threshold = 0.0;
        this.sharpness = 1.0;
        this.distortion = 1.0;

        // Properties that control the application range
        this.max_x = 1000;
        this.max_y = 1000;
        this.max_z = 1000;

        this.min_x = -1000;
        this.min_y = -1000;
        this.min_z = -1000;

        this._mask = null;

        // The geometry used to do the UV mapping
        this._geometry = null;

        // Resolution of the rendered texture
        this.resolution = 1024;

        this.initMaterials();

        Utilities.setValues(this, parameters);
    }

    get frequency() { return this._frequency; }
    set frequency(value) { this._frequency = new THREE.Vector3(value, value, value); }

    get frequency_x() { return this.frequency.x; }
    get frequency_y() { return this.frequency.y; }
    get frequency_z() { return this.frequency.z; }

    set frequency_x(value) { this.frequency.x = value; }
    set frequency_y(value) { this.frequency.y = value; }
    set frequency_z(value) { this.frequency.z = value; }

    get minTextureRange() { return new THREE.Vector3(this.min_x, this.min_y, this.min_z);}
    get maxTextureRange() { return new THREE.Vector3(this.max_x, this.max_y, this.max_z);}

    get geometry() {
        return this._geometry;
    }

    set geometry(value) {
        if (!value || !value.isGeometry) {
            console.error("ProceduralTexture: invalid geometry!");
            return;
        }

        this._geometry = value;
    }

    get mask() {
        return this._mask;
    }

    set mask(value) {
        if (!value || !value.isTexture) {
            this._mask = null;
            delete this.defines['USE_MASK'];
        } else {
            this._mask = value;
            this.defines['USE_MASK'] = '';
            this.uniforms.mask = new THREE.Uniform(value);
        }
    }

    get resolution() {
        return this.firstPassTarget.width;
    }

    set resolution(value) {
        this.firstPassTarget = new THREE.WebGLRenderTarget(value, value);
        this.secondPassTarget = new THREE.WebGLRenderTarget(value, value);
    }

    render() {
        this.updateUniforms();

        // First pass => render the procedural texture
        // Take into account the geometry of the model when rendering the texture
        if (this.geometry) {
            //GraphicUtilities.renderQuad(this.firstPassMaterial, this.firstPassTarget);
            GraphicUtilities.renderGeometry(this.geometry, this.firstPassMaterial, this.firstPassTarget);
        } else {
            GraphicUtilities.renderQuad(this.firstPassMaterial, this.firstPassTarget);
        }
        // Assign the result of the first pass to the material used in the second pass
        this.secondPassMaterial.uniforms.map.value = this.firstPassTarget.texture;

        // Second pass => fix UV seams
        GraphicUtilities.renderQuad(this.secondPassMaterial, this.secondPassTarget);

        return this.secondPassTarget.texture;
    }

    initMaterials() {
        this.uniforms = {
            frequency: new THREE.Uniform(this.frequency),
            displacement: new THREE.Uniform(this.displacement),
            threshold: new THREE.Uniform(this.threshold),
            sharpness: new THREE.Uniform(this.sharpness),
            distortion: new THREE.Uniform(this.distortion),
            maxTextureRange: new THREE.Uniform(this.maxTextureRange),
            minTextureRange: new THREE.Uniform(this.minTextureRange)
        };

        this.secondPassUniforms = {
            map: new THREE.Uniform(),
            texelOffset: new THREE.Uniform(1 / this.resolution)
        };

        this.defines = {
            'USE_MAP': '',
            'USE_PROCEDURAL_TEXTURE': ''
        };

        this.firstPassMaterial = new THREE.ShaderMaterial({
            uniforms: this.uniforms,
            defines: this.defines,
            vertexShader: CustomShaders['procedural_texture_vert'],
            fragmentShader: CustomShaders['procedural_texture_frag']
        });

        this.secondPassMaterial = new THREE.ShaderMaterial({
            uniforms: this.secondPassUniforms,
            defines: this.defines,
            vertexShader: CustomShaders['basic_vert'],
            fragmentShader: CustomShaders['second_pass_frag']
        });
    }

    updateUniforms() {
        this.uniforms.frequency = new THREE.Uniform(this.frequency);
        this.uniforms.displacement = new THREE.Uniform(this.displacement);
        this.uniforms.threshold = new THREE.Uniform(this.threshold);
        this.uniforms.sharpness = new THREE.Uniform(this.sharpness);
        this.uniforms.distortion = new THREE.Uniform(this.distortion);
        this.uniforms.maxTextureRange = new THREE.Uniform(this.maxTextureRange);
        this.uniforms.minTextureRange = new THREE.Uniform(this.minTextureRange);
        this.secondPassUniforms.texelOffset = new THREE.Uniform(1.0 / this.resolution);
    }

}
