isLoaded = [];

DemoSetup = {
    scene: null,
    camera: null,
    renderer: null,
    resources: [],
    width: 0,
    height: 0,

    /*
     * Demo initialization
     */
    init: function () {
        this.initScene();
        this.initCamera();
        this.initLights();
        this.initRenderer();

        window.addEventListener('resize', this.onWindowResize.bind(this));
    },

    initScene: function () {
        this.scene = new THREE.Scene();
        this.scene.fog = new THREE.Fog(0x303030, 10, 30);
        //this.scene.background = new THREE.Color( 0x303030 );

        let gridHelper = new THREE.GridHelper(50, 75);

        gridHelper.position.set(30, 0, 0);
        gridHelper.material.transparent = true;
        gridHelper.material.opacity = 0.1;

        this.scene.add(gridHelper);
    },

    initCamera: function () {
        this.width = Math.min(document.documentElement.clientWidth, window.innerWidth || 0);
        this.height = Math.min(document.documentElement.clientHeight, window.innerHeight || 0);

        let aspect = this.width / this.height;
        let fov = 65;

        if (aspect > 1.75) {
            fov = 1.63 * Math.pow(aspect, 2) - 22.43 * aspect + 98.9;
        }

        this.camera = new THREE.PerspectiveCamera(fov, aspect, 0.1, 1000);
        this.camera.position.set(7.5, 2.5, 0);
        this.camera.lookAt(new THREE.Vector3(0, 1.5, 0));
    },

    initRenderer: function () {
        this.renderer = new THREE.WebGLRenderer({
            alpha: true,
            antialias: true
        });

        this.renderer.setSize(this.width, this.height);
        this.renderer.setPixelRatio(1.0);
        this.renderer.setClearColor(this.scene.fog.color, 0);

        document.body.appendChild(this.renderer.domElement);

        GraphicUtilities.renderer = this.renderer;
    },

    initLights: function () {
        let ambientLight = new THREE.AmbientLight(0xffffff, 0.2);
        let frontLight = new THREE.DirectionalLight(0xf1e6d8, 1.0);
        let backLight = new THREE.DirectionalLight(0xb6ccf2, 2.0);

        frontLight.position.set(5, 2, 2);
        backLight.position.set(-10, 12, -2);

        this.scene.add(frontLight);
        this.scene.add(backLight);
        this.scene.add(ambientLight);
    },

    onWindowResize: function () {
        this.width = Math.min(document.documentElement.clientWidth, window.innerWidth || 0);
        this.height= Math.min(document.documentElement.clientHeight, window.innerHeight || 0);

        let aspect = this.width / this.height;
        let fov = 65;

        if (aspect > 1.75) {
            fov = 1.63 * Math.pow(aspect, 2) - 22.43 * aspect + 98.9;
        }

        this.camera.aspect = aspect;
        this.camera.fov = fov;
        this.camera.updateProjectionMatrix();

        this.renderer.setSize(this.width, this.height);
    },

    /*
     * Resource loading
     */
    loadResources: function (resourcesToLoad, onLoadCompletion) {
         for (let resource of resourcesToLoad) {
            switch (resource.type) {
                case 'texture':
                    this.loadTexture(resource.name, resource.path);
                    break;
                case 'model':
                    this.loadModel(resource.name, resource.path, resource.onLoad);
                    break;
                default:
                    console.error("DemoSetup: '" + resource.type + "' is not a valid resource type");
            }
        }

        this.checkProgress(resourcesToLoad, onLoadCompletion);
    },

    loadTexture: function (name, path) {
        let textureLoader;
        if(path.split('.').pop() == 'tga'){
            textureLoader = new THREE.TGALoader();
        }else{
            textureLoader = new THREE.TextureLoader();
        }

        isLoaded[name] = false;

        textureLoader.load(path, function (texture) {
            DemoSetup.resources[name] = texture;

            console.info("DemoSetup: '" + name + "' loaded!");
            isLoaded[name] = true;
        });
    },

    loadModel: function (name, path) {
        let loader = new THREE.JSONLoader();

        isLoaded[name] = false;

        loader.load(path, function (geometry, materials) {
            DemoSetup.resources[name] = {
                geometry: geometry,
                materials: materials
            };

            console.info("DemoSetup: '" + name + "' loaded!");
            isLoaded[name] = true;
        });
    },

    checkProgress: function (resourcesToLoad, onLoadCompletion) {
        for (let resource of resourcesToLoad) {
            if (!isLoaded[resource.name]) {
                setTimeout(this.checkProgress.bind(this, resourcesToLoad, onLoadCompletion), 100);
                return;
            }
        }

        // Hide loading screen
        let loadingScreen = document.getElementById('loading-screen');

        if (loadingScreen) {
            loadingScreen.style.visibility = 'hidden';
            loadingScreen.style.opacity = '0';
        }

        onLoadCompletion();
    },

    /*
     * Scene rendering
     */
    renderScene: function () {
        this.renderer.render(this.scene, this.camera);
    },
};