//Main model data
let modelpath = '../model/horse_rigged.json';
let geompath = '../model/horse_geometry.json';
let texturepath = '../model/textures/horse_texture.jpg';

//Additional information
let numMat = 1;
let scalenum = 1;

//Mesh modifier
let scaleX = ['belly','spine.004','shoulder.L','shoulder.R'];
let scaleY = ['belly','root'];
let scaleZ = ['tail.001','tail.002','spine.002'];

//Texture modifier
let allcolors = false;
let noise = true;
let freq = 0.05;