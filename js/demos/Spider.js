//Main model data
let modelpath = '../model/cosa.json';
let texturepath = '../model/dragon_C.jpg';

//Additional information
let numMat = 0;
let scalenum = 0.5;

//Mesh modifier
let scaleX = [];//= ['tail_1','tail_2','tail_3','ACT_neck_1','ACT_neck_2','ACT_neck_3'];
let scaleY = [];//= ['ik_underarm_L', 'ik_underarm_R', 'back_food_L', 'back_food_R'];
let scaleZ = [];//= ['w1_L','w1_R','w4_L','w4_R','w3_L','w3_R','w6_L','w6_R'];

//Texture modifier
let allcolors = true;
let noise = false;
let freq = 0.3;