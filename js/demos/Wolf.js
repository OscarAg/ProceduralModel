//Main model data
let modelpath = '../model/WolfThree.json';
let texturepath = '../model/textures/wolf_full2.jpg';

//Additional information
let numMat = 0;
let scalenum = 7;

//Mesh modifier
let scaleX = ['Hals','Brust'];
let scaleY = ['Kopf','Schwanz'];
let scaleZ = ['Becken'];

//Texture modifier
let allcolors = false;
let noise = true;
let freq = 0.05;