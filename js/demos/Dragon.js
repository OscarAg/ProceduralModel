//Main model data
let modelpath = '../model/ThreeDrachen.json';
let texturepath = '../model/textures/dragon_C.jpg';
let normalmappath = '../model/textures/dragon_N.jpg';
let specularmappath = '../model/textures/dragon_S.jpg';

//Additional information
let numMat = 0;
let scalenum = 0.11;

//Mesh modifier
let scaleX = ['w1_L','w4_L','w7_L','w1_R','w4_R','w7_R',];
let scaleY = ['pelvic_2'];
let scaleZ = ['head','tail_1'];

//Texture modifier
let allcolors = true;
let noise = true;
let freq = 0.3;