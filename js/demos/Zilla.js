//Main model data
let modelpath = '../model/Zilla/ThreeZilla.json';
let texturepath = '../model/Zilla/skin-comb.jpg';
let normalmappath = '../model/Zilla/skin-n.tga';
let alphamappath = '../model/Zilla/skin-a.tga';

//Additional information
let numMat = 0;
let scalenum = 0.05;

//Mesh modifier
let scaleX = ['neck','head','jaw','back'];
let scaleY = ['neck','control'];
let scaleZ = ['control'];

//Texture modifier
let allcolors = true;
let noise = true;
let freq = 10;