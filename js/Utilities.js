Utilities = {

    /*
     * Binds the value of the target property to the value of the source property,
     * so that changes in the source objects are also reflected in the target.
     */
    bindProperty: function (source, sourceProperty, target, targetProperty, func = (x) => x) {
        if (!source || !target) {
            console.error("Utilities: cannot bind property (source or target missing)");
            return;
        }

        if (source[sourceProperty] === undefined) {
            console.warn("Utilities: '" + sourceProperty + "' is not a property of " + source.constructor.name);
        }

        if (target[targetProperty] === undefined) {
            console.warn("Utilities: '" + targetProperty + "' is not a property of " + target.constructor.name);
        }

        // Bind the getter of the target property to the source object's property
        Object.defineProperty(target, targetProperty, {
            get: function () {
                return func(source[sourceProperty]);
            }
        });
    },

    /*
     * Changes the property of the source object so that it returns a random number
     * from a uniform distribution each time it is read.
     *
     * A second property is defined (preceded by an underscore) to access the value
     * without triggering the randomization.
     */
    randomizeFloat: function (source, property, min = 0, max = 1) {
        if ((typeof min !== "number") || (typeof max !== "number")) {
            console.error("Utilities: the values of 'min' and 'max' have to be numeric");
            return;
        }

        if (!source) {
            console.error("Utilities: cannot randomize float (source missing)");
            return;
        }

        if (source[property] === undefined) {
            console.warn("Utilities: '" + property + "' is not a property of " + source.constructor.name);
        }

        // Replace the getter of the property with a function that generates random numbers
        Object.defineProperty(source, property, {
            get: function () {
                source['_' + property] = Utilities.random(min, max);
                return source['_' + property];
            }
        });

        // Read the property to initialize its value
        source[property];
    },

    /*
     * Changes the property of the source object so that it returns a random number
     * from a normal distribution each time it is read.
     *
     * A second property is defined (preceded by an underscore) to access the value
     * without triggering the randomization.
     */
    randomizeFloatNormal: function (source, property, mean = 0, sd = 1) {
        if ((typeof mean !== "number") || (typeof sd !== "number")) {
            console.error("Utilities: the values of 'mean' and 'sd' have to be numeric");
            return;
        }

        if (!source) {
            console.error("Utilities: cannot randomize float (source missing)");
            return;
        }

        if (source[property] === undefined) {
            console.warn("Utilities: '" + property + "' is not a property of " + source.constructor.name);
        }

        // Replace the getter of the property with a function that generates random numbers
        Object.defineProperty(source, property, {
            get: function () {
                source['_' + property] = Utilities.randomNormal(mean, sd);
                return source['_' + property];
            }
        });

        // Read the property to initialize its value
        source[property];
    },

    /*
     * Changes the given property so that it returns random boolean values
     * with the specified probability of being true (by default 0.5).
     *
     * A second property is defined (preceded by an underscore) to access the value
     * without triggering the randomization.
     */
    randomizeBool: function (source, property, probability = 0.5) {
        if (typeof probability !== "number") {
            console.error("Utilities: the value of 'probability' has to be numeric");
            return;
        }

        if (!source) {
            console.error("Utilities: cannot randomize boolean (source missing)");
            return;
        }

        if (source[property] === undefined) {
            console.warn("Utilities: '" + property + "' is not a property of " + source.constructor.name);
        }

        // Replace the getter of the property with a function that generates random booleans
        Object.defineProperty(source, property, {
            get: function () {
                source['_' + property] = (Math.random() < probability);
                return source['_' + property];
            }
        });

        // Read the property to initialize its value
        source[property];
    },

    /*
     * Generates pseudo-random numbers uniformly distributed in the range [min, max]
     */
    random: function (min = 0, max = 1) {
        return Math.random() * (max - min) + min;
    },

    /*
     * Generates pseudo-random numbers from a normal distribution N(mean, sd) using the Box-Muller method
     */
    randomNormal: function (mean = 0, sd = 1) {
        let u1 = Math.random();
        let u2 = Math.random();

        let random = Math.sqrt(-2.0 * Math.log(u1)) * Math.cos(2.0 * Math.PI * u2);

        return sd * random + mean;
    },

    /*
     * Sets the values of the properties of the object with the values contained in 'values'
     */
    setValues: function (object, values) {
        if (!values || !object) return;

        for (let key in values) {
            if (!values.hasOwnProperty(key)) continue;

            let newValue = values[key];

            if (newValue === undefined) {
                console.warn("Utilities: value of '" + key + "' undefined");
                continue;
            }

            if (object[key] === undefined) {
                console.warn("Utilities: '" + key + "' is not a property of " + object.constructor.name);
                continue;
            }

            object[key] = newValue;
        }
    }
};

GraphicUtilities = {

    renderer: null,

    /*
     * Renders a quad with the given material and stores the result in the specified render target
     */
    renderQuad: function (material, renderTarget) {
        if (!this.renderer) {
            console.error("GraphicUtilities: cannot render quad (renderer missing)");
            return;
        }

        if (!material || !material.isMaterial) {
            console.error("GraphicUtilities: cannot render quad (invalid material)");
            return
        }

        let scene = new THREE.Scene();
        let camera = new THREE.OrthographicCamera();
        let quad = new THREE.Mesh(new THREE.PlaneGeometry(2, 2, 2, 2), material);

        scene.add(quad);

        this.renderer.render(scene, camera, renderTarget);
    },

    /*
     * Renders the geometry with the given material and stores the result in the specified render target
     */
    renderGeometry: function (geometry, material, renderTarget) {
        if (!this.renderer) {
            console.error("GraphicUtilities: cannot render geometry (renderer missing)");
            return;
        }

        if (!geometry || !geometry.isGeometry) {
            console.error("GraphicUtilities: cannot render geometry (invalid geometry)");
            return
        }

        if (!material || !material.isMaterial) {
            console.error("GraphicUtilities: cannot render geometry (invalid material)");
            return
        }

        let scene = new THREE.Scene();
        let camera = new THREE.OrthographicCamera();
        let mesh = new THREE.Mesh(geometry, material);

        scene.add(mesh);

        this.renderer.render(scene, camera, renderTarget);
    },

    /*
     * Renders the texture on the lower left corner of the viewport
     */
    renderTexture: function (texture, size = 300) {
        if (!this.renderer) {
            console.error("GraphicUtilities: cannot render geometry (renderer missing)");
            return;
        }

        let scene = new THREE.Scene();
        let camera = new THREE.OrthographicCamera(-1, 1, 1, -1, -1, 1);
        let quad = new THREE.Mesh(new THREE.PlaneGeometry(2, 2, 10, 10), new THREE.MeshBasicMaterial());

        quad.material.map = texture;

        scene.add(quad);

        // Change viewport and disable auto clear to draw on top of the last frame
        this.renderer.setViewport(5, 5, size, size);
        this.renderer.autoClear = false;

        // Render the texture
        this.renderer.render(scene, camera);

        // Restore viewport and re-enable auto clear
        this.renderer.setViewport(0, 0, window.innerWidth, window.innerHeight);
        this.renderer.autoClear = true;
    },

    /*
     * Returns the capabilities of the renderer
     */
    getCapabilities: function () {
        if (!this.renderer) {
            console.error("GraphicUtilities: cannot get capabilities (renderer missing)");
            return;
        }

        return this.renderer.capabilities;
    }

};
