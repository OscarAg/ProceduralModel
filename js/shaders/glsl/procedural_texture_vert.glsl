varying vec2 vUv;
varying vec3 vertexPosition;

void main() {
    vUv = uv;
    vertexPosition = position;

    // Transform uv coordinates [0, 1] to normalized device coordinates [-1, 1]
    gl_Position = vec4( 2.0*uv - 1.0, 0.0, 1.0 );
}