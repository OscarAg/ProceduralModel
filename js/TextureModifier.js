class TextureModifier extends Modifier {

    constructor(parameters) {
        super();

        this.lightness = 0;
        this.saturation = 0;
        this.hue = 0;
        this.value = 0;

        this.masks = [];

        Utilities.setValues(this, parameters);
    }

    apply(target) {
        if (!TextureModifier.isValid(target)) return;

        // Update the material used to apply the filter
        this.updateMaterial(target.map);

        // Get a render target from the resource manager
        let renderTarget = ResourceManager.getRenderTarget();

        // Apply the filter
        GraphicUtilities.renderQuad(this.colorFilterMaterial, renderTarget);

        // Set the filtered texture to the target
        ResourceManager.bindTexture(target, renderTarget);
    }

    updateMaterial(texture) {
        let lightness = this.lightness;
        let brightness = 0.25 * lightness;
        let contrast = 0.40 * lightness + 1.0;

        let uniforms = {
            brightness: new THREE.Uniform(brightness),
            contrast: new THREE.Uniform(contrast),
            hue: new THREE.Uniform(this.hue),
            saturation: new THREE.Uniform(this.saturation),
            value: new THREE.Uniform(this.value),
            texture: new THREE.Uniform(texture)
        };

        let defines = {
            'USE_COLOR_FILTER': '',
        };

        if (this.masks && this.masks.length > 0) {
            defines['USE_MASK'] = '';
            uniforms.mask = new THREE.Uniform(this.blendMasks());
        }

        this.colorFilterMaterial = new THREE.ShaderMaterial({
            uniforms: uniforms,
            defines: defines,
            vertexShader: CustomShaders['basic_vert'],
            fragmentShader: CustomShaders['color_filter_frag']
        });
    }

    blendMasks() {
        let masks = [];

        // Render the procedural masks
        for (let mask of this.masks) {
            if (mask.isTexture) masks.push(mask);
            else if (mask.isProceduralTexture) masks.push(mask.render());
        }

        if (masks.length === 0) return null;
        if (masks.length === 1) return masks[0];

        let textureBlender = new TextureBlender();

        return textureBlender.blend(masks);
    }

    static isValid(target) {
        if (!target) {
            console.error("TextureModifier: cannot apply modifier (material missing)");
            return false;
        }

        if (!target.isMaterial) {
            console.error("TextureModifier: cannot apply modifier (target is not a material)");
            return false;
        }

        return true;
    }

}